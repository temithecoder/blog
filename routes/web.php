<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

// Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/home', [PostController::class,   'index']);

Route::middleware(['auth'])->group(function(){
    Route::get('new-post', [PostController::class,   'create']);
    Route::post('new-post', [PostController::class,   'store']);
    Route::get('edit/{slug}', [PostController::class, 'edit']);
    Route::post('update', [PostController::class, 'update']);
    Route::get('delete/{id}', [PostController::class, 'destroy']);
    // Route::get('my-all-posts', '')
    Route::post('comment/add', [CommentController::class,  'store']);
    Route::post('comment/delete/{id}',  [CommentController::class, 'destory']);

    //user profile
    Route::get('user/{id}',   [UserController::class, 'profile'])->where('id', '[0-9]+');
    //display list of posts
    Route::get('user/{id}/posts', [UserController::class, 'user_posts'])->where('id', '[0-9]+');
    //display single post
    Route::get('/{slug}', ['as'=>'post', 'uses'=>[PostController::class, 'show']])->where('slug', '[A-Za-z0-9-_]+');
});