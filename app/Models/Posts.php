<?php
namespace App\Models;
Use App\Models\User;
use App\Models\Comments;

USe Illuminate\Database\Eloquent\Model;

class Posts extends Model{
    protected $guarded =  [];
    public function comments()
    {
        return $this->hasMany('Comments', 'on_post');
    }
    public function author(){
        return  $this->belongsTo(User::class);
    }
}
?>